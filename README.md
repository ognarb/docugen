Dockerfile for generating pdf from pandoc and latex

Docker container image for generating documentation.

Contains:

* pandoc
* latex
* latexmk

## Project using this Dockerfile

* [Program analysus cheatsheet](https://gitlab.com/ognarb/program-analysis-cheatsheet)
* [« Networking, The Net as Artwork » (frensh translation)](https://gitlab.com/net-as-artwork/networking)

Instructions
------------

### Interactive

To pull this container from the Docker registry:

```
$ docker pull ognarb/docugen
```

or

```
$ docker pull registry.gitlab.com/ognarb/docugen
```

and run `pandoc` or `latexmk` interactive.


### Gitlab CI runner

This container can also be used as a Gitlab CI runner. Use the following
instructions inside `.gitlab-ci.yml` for either `pandoc`:

```
image: registry.gitlab.com/ognarb/docugen:latest

pages:
  script:
  - mkdir public
  - pandoc -t html5 -o public/index.html ./md/index.md
  - latexmk ./document.tex
  artifacts:
    paths:
    - public
  only:
  - master
```


Authors
-------

You can contact me on the fediverse [@carl@linuxrocks.online](https://linuxrocks.online/@carl).
